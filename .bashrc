#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#{{{ aliases
# coreutils
alias ls='ls --color=auto'
alias ll='ls --color=auto -l'
alias la='ls --color=auto -a'
alias lla='ls --color=auto -la'
alias lh='ls --color=auto -lh'
alias grep='grep --color=auto'
alias info='info --vi-keys'
alias cdd='cd ..'
alias cddd='cd ../..'
alias psmine='ps -u $(whoami)'

# pacman
alias paci='pacman -Si'
alias pacs='pacman -Ss'
alias pacq='pacman -Qi'
alias FML='sudo pacman -Syu'

# scm
alias gits='git status -s -b -uno'
alias gitsu='git status -s -b -unormal'
alias gitd='git diff'
alias gitdc='git diff --cached'
alias gitl='git log'
alias gitll='git log --stat'
alias gitlp='git log -p'

# Other random stuff
alias mo='mimeo'
alias ff='(firefox &> /dev/null && pkill at-spi &)'
alias fp='(firefox -private &> /dev/null && pkill at-spi &)'

#}}}

#{{{ environment variables
[[ -n $SSH_TTY ]] && \
	PS1='\[\e[1m\][\u@\h \W]\$\[\e[0m\] ' || PS1='[\[\e[32m\]\W\[\e[0m\]]\$ '
	# PS1='\[\e[1m\][\u@\h \W]\$\[\e[0m\] ' || PS1='[\[\e[32m\]\@\[\e[0m\] \W]\$ '
eval $(dircolors -b)
export HISTIGNORE="&:ls:ll:la:lla:cd:exit*:acpi:sensors:vmail*:lh:gits:
:gitsu:gitd:gitl:gitll:gitlp:gitb:hgs:hgd:hgl:hgll:hglp:ff:exit:pwd:ghci"
export HISTSIZE=20000
export HISTFILESIZE=20000
export HISTCONTROL=ignoreboth
export EDITOR=vim
export SUDO_EDITOR="/usr/bin/vim -p -X"
unset PROMPT_COMMAND
export FREETYPE_PROPERTIES="truetype:interpreter-version=35 cff:no-stem-darkening=0 autofitter:warping=1"
export LC_COLLATE=C
#}}}

#{{{ color setup
# if [ "$TERM" = "linux" ]; then
# 	eval `dircolors ~/.dircolors_ansi`
# else
# 	eval `dircolors ~/.dircolors_256`
# fi

if [[ -n ${XTERM_SHELL+1} ]] || [[ -f .usecolor ]] && [[ $TERM != "linux" ]]
then
	eval `dircolors ~/.dircolors_256`
fi

# experimental colored man pages:

man() {
	env \
		LESS_TERMCAP_mb=$(printf "\e[1;31m") \
		LESS_TERMCAP_md=$(printf "\e[1;31m") \
		LESS_TERMCAP_me=$(printf "\e[0m") \
		LESS_TERMCAP_se=$(printf "\e[0m") \
		LESS_TERMCAP_so=$(printf "\e[1;42;30m") \
		LESS_TERMCAP_ue=$(printf "\e[0m") \
		LESS_TERMCAP_us=$(printf "\e[1;35m") \
			man "$@"
}
#}}}

#{{{ random uncategorized functions

edit() {
	(/usr/bin/scite "$@" &> /dev/null &)
}

# Toggle synaptics touchpad on/off:
tmouse() {
	local newval=0
	synclient -l | grep -q 'TouchpadOff.*=.*0' && let newval=2
	synclient TouchpadOff=$newval
}

# test new screen resolution:
modetest() {
	[[ -z "$1" ]] && \
		echo "please supply a mode. Consult xrandr -q for options." && \
		return 1
	echo "trying mode $1..."
	xrandr --output VGA-1 --mode "$1"
	for (( i = 0; i < 5; i++ )); do
		printf "reverting in %2s seconds... (ctrl-c to keep new settings)\n" \
			"$(( 5 - i ))"
		sleep 1
	done
	xrandr --output VGA-1 --auto
}

zo() {
	(zathura --fork "$@" &> /dev/null && pkill at-spi &)
}

zl() {
	local f="$(ls -t /tmp/*.pdf | head -1)"
	echo "Opening $f"
	(zathura --fork "$f" &> /dev/null && pkill at-spi &)
}

cal() {
	if [[ $1 == "-3" && $# == 1 ]]; then
		command cal $@ | sed -r -e \
		's/(.{22,43})\b('$(date +%-e)')\b(.{22,})/\1\o033[1;31m\2\o033[0m\3/'
	elif (( $# == 0 )); then
		command cal $@ | sed -r -e \
			's/\b('$(date +%-e)')\b/\o033[1;31m\1\o033[0m/g'
	else
		command cal $@
	fi
	# TODO: find clean solution for -y...
}

throwaway_c() {
	local tdir=$(mktemp -d /tmp/deleteme-XXX)
	cd $tdir
	cp ~/.vim/skeletons/skeleton.make Makefile
	cat > test.c <<"EOF"
#include <stdio.h>
#include <stdlib.h>

int main() {
	return 0;
}
EOF
	vim test.c
}

throwaway_cpp() {
	local tdir=$(mktemp -d /tmp/deleteme-XXX)
	cd $tdir
	sed -e 's/LD\s*:=.*$/LD       := $(CXX)/' \
		< ~/.vim/skeletons/skeleton.make > Makefile
	cat > test.cpp <<"EOF"
#include <stdio.h>
#include <stdlib.h>

int main() {
	return 0;
}
EOF
	vim test.cpp
}

#}}}

#{{{ completion stuff

complete -c which
complete -d cd
complete -cf sudo

#{{{  shortcuts / bookmarks
j() {
	cd -P ~/.config/marks/$1 2> /dev/null || echo "Mark [$1] not set."
}

mark() {
	[[ -d ~/.config/marks/ ]] || mkdir -p "~/.config/marks/"
	[[ -z $1 ]] && markname=$(basename $PWD) || markname=$1
	ln -sv $PWD ~/.config/marks/"$markname"
}

unmark() {
	local d=${1%%/}
	[[ -z "$d" ]] && echo "Please enter mark." && return 1
	rm -v ~/.config/marks/"$d"
}

_marks_show() {
	local cur=${COMP_WORDS[COMP_CWORD]} # stuff typed so far?
	COMPREPLY=($(cd ~/.config/marks/ && ls -d $cur*/ 2> /dev/null)) \
		|| COMPREPLY=()
}

complete -o default -o nospace -F _marks_show j
complete -o default -F _marks_show unmark
#}}}

_pdfps_show() {
	local cur=${COMP_WORDS[COMP_CWORD]} # stuff typed so far
	# NOTE: seems more natural to filter via ls *.{pdf,ps}, but when there
	# aren't any matches for one or the other, you get a bad return code
	# and are dumped into the || section...
	local ifs=$IFS
	IFS=$'\n' COMPREPLY=($(ls -dp $cur* 2> /dev/null | grep -i '\(\.pdf\|\.ps\|/\)$')) \
		|| COMPREPLY=()
	IFS=$ifs
	# NOTE: if the filename has spaces and whatnot, you'll need to quote it.
}

complete -o default -o nospace -F _pdfps_show zo

#}}}

# vim:foldmethod=marker:foldmarker={{{,}}}
